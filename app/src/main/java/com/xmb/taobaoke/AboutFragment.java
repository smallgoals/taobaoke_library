package com.xmb.taobaoke;

import android.app.ui.FeedbackUI;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nil.sdk.ui.BaseFragmentV4;
import com.nil.sdk.ui.BaseUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.xmb.about.AboutPage;
import com.xmb.about.Element;

public class AboutFragment extends BaseFragmentV4 {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        simulateDayNight(/* DAY */ 0);

        View aboutPage = new AboutPage(getActivity())
                .isRTL(false)
                .setDescription(getString(R.string.app_name))
                .setImage(R.drawable.tbk_ic_launcher)
                .addGroup("版本")
                .addItem(new Element().setTitle("Version " + getAppVersionName(getActivity()))
                        .setOnLongClickListener(new View
                                .OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                String qudao = getString(R.string.proxy_channel);
                                new AlertDialog.Builder(getActivity()).setTitle("proxy:" + qudao)
                                        .setMessage
                                        (BaseUtils
                                                .getAppInfo
                                                        (getActivity()) + "\n" + AdSwitchUtils
                                                .getInstance
                                                        (getActivity())
                                                .getAdSwString()).setPositiveButton("ok", null)
                                        .show();
                                return false;
                            }
                        }))
                .addGroup("关于")
                .addFeedback("投诉建议", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FeedbackUI.FeedbackVo fd2 = new FeedbackUI.FeedbackVo("taobaoke",
                                FeedbackUI.FeedbackDiy1.Feedback.name(), "问题与反馈");
                        FeedbackUI.start(getActivity(), fd2);
                    }
                })// 自定义文字：addFeedback("意见反馈")
//                .addFiveStar()//自定义文字：addFeedback(title)
//                .addEmail("support@xiaomubiaoke.com")//自定义文字：addEmail(email, title)
                .addWarning(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        WebHtmlActivity.start(getActivity(), "软件协议与声明",
                                getString(R.string.url_agreement));
                    }
                })// or: addWarning(String title)
                .addCheckUpdate(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        BaseUtils.checkUpdate(getActivity(), true);
                    }
                })// or: addCheckUpdate(title)
                .addShare("分享APP", "发现一个好APP：" + getActivity().getApplicationInfo().loadLabel
                        (getActivity().getPackageManager
                                ()))
                .addCopyRights()
                .create();

        return aboutPage;
    }

    public String getAppVersionName(Context context) {
        String versionName = "";
        try {
            versionName = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0).versionName;
        } catch (Exception e) {
        }
        return versionName;
    }
}
