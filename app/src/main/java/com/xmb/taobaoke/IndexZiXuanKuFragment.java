package com.xmb.taobaoke;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.nil.sdk.nb.utils.NbDensityUtils;
import com.nil.sdk.ui.BaseFragmentV4;
import com.squareup.picasso.Picasso;
import com.taobao.api.domain.UatmTbkItem;
import com.taobao.api.response.TbkUatmFavoritesItemGetResponse;
import com.xmb.taobaoke.bean.BaseIndexBean;
import com.xmb.taobaoke.bean.ServerConfigBean;
import com.xmb.taobaoke.util.ServerConfigDB;
import com.xmb.taobaoke.web.MTA;
import com.xmb.taobaoke.web.XMBApi;
import com.xmb.taobaoke.web.XMBApiCallback;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;
import com.zhy.adapter.recyclerview.wrapper.HeaderAndFooterWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

public class IndexZiXuanKuFragment extends BaseFragmentV4 implements
        XMBApiCallback<TbkUatmFavoritesItemGetResponse>, View.OnClickListener {
    RecyclerView rv;
    ImageView btnTop;
    ImageView ivSearch;
    RelativeLayout btnSearch;
    SpinKitView loading;
    private TbkUatmFavoritesItemGetResponse tbkUatmFavoritesItemGetResponse;
    private List<UatmTbkItem> list = new ArrayList<UatmTbkItem>();
    private MyAdapter myAdapter;
    private HeaderAndFooterWrapper headViewAdapter;
    private Banner lunbo;
    private int screenWidth;
    private int ivHeightPx;
    private int itemMarginPx;
    private ServerConfigBean configBean;

    public static IndexZiXuanKuFragment newInstance(String favoritesType) {
        IndexZiXuanKuFragment f = new IndexZiXuanKuFragment();
        Bundle b = new Bundle();
        b.putCharSequence("favoritesType", favoritesType);
        f.setArguments(b);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tbk_fragment_index_zixuanku, container, false);
//        unbinder = ButterKnife.bind(this, rootView);
        configBean = ServerConfigDB.get();

        rv = rootView.findViewById(R.id.rv);
        btnTop = rootView.findViewById(R.id.btn_top);
        btnTop.setOnClickListener(this);
        ivSearch = rootView.findViewById(R.id.iv_search);
        btnSearch = rootView.findViewById(R.id.btn_search);
        btnSearch.setOnClickListener(this);
        loading = rootView.findViewById(R.id.loading);


        //为轮播图片长宽：
        screenWidth = ScreenUtils.getScreenWidth();
        ivHeightPx = NbDensityUtils.dp2px(getActivity(), 190);
        itemMarginPx = NbDensityUtils.dp2px(getActivity(), 4);

        myAdapter = new MyAdapter(getContext(), R.layout.tbk_index_recycle_view_item, list);

        // 设置HeadView：
        headViewAdapter = new HeaderAndFooterWrapper(myAdapter);
        View headview = inflater.inflate(R.layout.tbk_index_headview, null);
        lunbo = headview.findViewById(R.id.lunbo);
        headViewAdapter.addHeaderView(headview);

        final GridLayoutManager layoutManager = new GridLayoutManager(this.getContext(), 2);
        //设置布局管理器
        rv.setLayoutManager(layoutManager);
        //设置为垂直布局，这也是默认的
        layoutManager.setOrientation(OrientationHelper.VERTICAL);
        //设置Adapter
        rv.setAdapter(headViewAdapter);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setHasFixedSize(true);//固定大小,提高性能
        //设置滑动到哪个位置了的监听
        final int height = ScreenUtils.getScreenHeight();
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int adapterNowPos = layoutManager.findFirstVisibleItemPosition();
                if (adapterNowPos <= 10) {
                    btnTop.setVisibility(View.GONE);
                } else {
                    btnTop.setVisibility(View.VISIBLE);
                }
            }
        });
        initLunbo();
        initCategory(headview);
        return rootView;
    }

    private void initCategory(View rootView) {
        final List<BaseIndexBean> list = configBean.getIndex_category();
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        ViewGroup line1 = rootView.findViewById(R.id.layout_category_line_1);
        ViewGroup line2 = rootView.findViewById(R.id.layout_category_line_2);

        for (int i = 0; i < list.size(); i++) {
            View view = inflater.inflate(R.layout.tbk_item_category, i < 4 ? line1 : line2, false);
            ImageView iv = view.findViewById(R.id.iv_category);
            Picasso.with(getContext()).load(list.get(i).getImgUrl()).into
                    (iv);
            TextView tv = view.findViewById(R.id.tv_category);
            tv.setText(list.get(i).getTitle());
            final int j = i;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBaseIndexCategoryClick(list.get(j));
                }
            });
            if (i < 4) {
                line1.addView(view);
            } else {
                line2.addView(view);
            }
        }
    }

    private void initLunbo() {
        lunbo.setDelayTime(5000);

        final List<BaseIndexBean> list = configBean.getIndex_lunbo();
        List<String> imgs = new ArrayList<>();
        for (BaseIndexBean indexLunbo : list) {
            imgs.add(indexLunbo.getImgUrl());
        }
        lunbo.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
                onBaseIndexCategoryClick(list.get(position));
            }
        });
        if (imgs != null) {
            //设置图片加载器
            lunbo.setImageLoader(new LunboImageLoader());
            //设置图片集合
            lunbo.setImages(imgs);
            //banner设置方法全部调用完毕时最后调用
            lunbo.start();
        }
    }

    private void onBaseIndexCategoryClick(BaseIndexBean baseIndexBean) {

        //事件统计;
        Map<String, String> map = new HashMap<>();
        map.put("event_name", "进入小分类");
        map.put("diy1", baseIndexBean.getType());
        map.put("diy2", baseIndexBean.getTitle());
        map.put("diy3", baseIndexBean.getData());
        MTA.sendEvent(map);

        if (baseIndexBean.getType().equals(BaseIndexBean.TYPE_URL2MYSELF)) {
            WebHtmlActivity.start(getActivity(), baseIndexBean.getTitle(), baseIndexBean
                    .getData());
        } else if (baseIndexBean.getType().equals(BaseIndexBean.TYPE_URL2TAOBAO)) {
            //判断 淘宝App 是否安装:
            boolean isTBInstall = AppUtils.isInstallApp("com.taobao.taobao");
            //如果未安装淘宝，则直接用浏览器领券：
            if (!isTBInstall) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri content_url = Uri.parse(baseIndexBean.getData());
                intent.setData(content_url);
                startActivity(intent);
                return;
            }

            String tbUrl = baseIndexBean.getData().replace("https://", "");
            tbUrl = tbUrl.replace("http://", "");
            tbUrl = "taobao://" + tbUrl;
            Intent intent =
                    new Intent();
            intent.setAction("android.intent.action.VIEW");
            Uri uri = Uri.parse(tbUrl);
            intent.setData(uri);
            startActivity(intent);
        } else if (baseIndexBean.getType().equals(BaseIndexBean.TYPE_ZIXUANKU)) {
            ZiXuanKuActivity.start(getActivity(), baseIndexBean.getTitle(), baseIndexBean.getData
                    ());
        } else {

        }
    }


    public class LunboImageLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            //Picasso 加载图片简单用法
            Picasso.with(context).load((String) path).resize(screenWidth, ivHeightPx).centerCrop
                    ().into
                    (imageView);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (tbkUatmFavoritesItemGetResponse == null) {
            String favoritesType = getArguments().getCharSequence("favoritesType").toString();
            XMBApi.zixuanku(this, favoritesType);
        }
    }

    @Override
    public void onFailure(Call call, final Exception e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loading.setVisibility(View.GONE);
                ToastUtils.showLong("加载失败，请重试");
            }
        });

    }

    @Override
    public void onResponse(final TbkUatmFavoritesItemGetResponse obj, Call call, Response
            response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tbkUatmFavoritesItemGetResponse = obj;
                list = tbkUatmFavoritesItemGetResponse.getResults();
                if (list != null) {
                    myAdapter.getDatas().clear();
                    myAdapter.getDatas().addAll(list);
                    headViewAdapter.notifyDataSetChanged();
                }
                loading.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btn_search) {
            ActivityUtils.startActivity(SearchActivity.class);

        } else if (i == R.id.btn_top) {
            rv.scrollToPosition(0);

        }
    }

    private class MyAdapter extends CommonAdapter<UatmTbkItem> {


        public MyAdapter(Context context, int layoutId, List datas) {
            super(context, layoutId, datas);
        }

        @Override
        protected void convert(ViewHolder holder, final UatmTbkItem item, int position) {
            Picasso.with(getContext())
                    .load(item.getPictUrl())
                    .placeholder(R.drawable.tbk_item_default)
                    .into((ImageView) holder.getView(R.id.iv_pic));
            holder.setText(R.id.tv_title, item.getTitle());
            holder.setText(R.id.tv_price, "现价￥" + item.getZkFinalPrice());
            double coupon = formatCouponInfo(item.getCouponInfo());
            holder.setText(R.id.tv_coupon, subZeroAndDot(coupon + "") + "元");
            double priceNow = Double.parseDouble(item.getZkFinalPrice()) - coupon;
            holder.setText(R.id.tv_price_after, "￥" + subZeroAndDot(String.format("%.2f",
                    priceNow)));
            holder.setText(R.id.tv_volume, "月销 " + item.getVolume());

            //如果没券：
            boolean hasCoupon = !StringUtils.isEmpty(item.getCouponClickUrl());
            holder.setVisible(R.id.tv_price, hasCoupon);
            holder.setVisible(R.id.layout_coupon, hasCoupon);
            holder.setVisible(R.id.tv_quanhoujia, hasCoupon);

            holder.setOnClickListener(R.id.layout, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ItemDetailActivity.start(getActivity(), item);
                }
            });

            RelativeLayout layout = holder.getView(R.id.layout);
            if (position % 2 == 0) {
                layout.setPadding(itemMarginPx / 2, 0, itemMarginPx, itemMarginPx);
            } else {
                layout.setPadding(itemMarginPx, 0, itemMarginPx / 2, itemMarginPx);
            }
        }

        private double formatCouponInfo(String couponInfo) {
            if (StringUtils.isEmpty(couponInfo)) return 0;
            if (couponInfo.indexOf("减") != -1 && couponInfo.lastIndexOf("元") == couponInfo.length
                    () - 1) {
                couponInfo = couponInfo.substring(couponInfo.indexOf("减") + 1, couponInfo
                        .lastIndexOf("元"));
                try {
                    return Double.parseDouble(couponInfo);
                } catch (NumberFormatException e) {
                    return 0;
                }
            }
            return 0;
        }

        /**
         * 使用java正则表达式去掉多余的.与0
         *
         * @param s
         * @return
         */
        public String subZeroAndDot(String s) {
            if (s.indexOf(".") > 0) {
                s = s.replaceAll("0+?$", "");//去掉多余的0
                s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
            }
            return s;
        }
    }

}
