package com.xmb.taobaoke;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.squareup.picasso.Picasso;
import com.taobao.api.domain.UatmTbkItem;
import com.taobao.api.response.TbkDgItemCouponGetResponse;
import com.xmb.taobaoke.web.MTA;
import com.xmb.taobaoke.web.XMBApi;
import com.xmb.taobaoke.web.XMBApiCallback;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.loader.ImageLoader;

import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

public class ItemDetailActivity extends BaseAppCompatActivity implements XMBApiCallback<String>,
        View.OnClickListener {

    Banner lunbo;
    ImageView back;
    TextView tvVolume;
    TextView tvPriceAfter;
    TextView tvPrice;
    TextView tvCoupon;
    TextView tvTkl;
    TextView btnCopy;
    TextView btnOpen;
    LinearLayout layoutBottom;
    TextView tvTitle;
    LinearLayout layoutCoupon;
    TextView tvTklTips;
    RelativeLayout layoutShare;
    RelativeLayout layoutOpen;
    private TbkDgItemCouponGetResponse.TbkCoupon tbkCoupon;
    private UatmTbkItem uatmTbkItem;
    private boolean isTBKCoupon;
    private String tkl;

    public static void start(Activity act, TbkDgItemCouponGetResponse.TbkCoupon tbkCoupon) {
        Intent it = new Intent(act, ItemDetailActivity.class);
        it.putExtra("tbkCoupon", tbkCoupon);
        act.startActivity(it);
    }

    public static void start(Activity act, UatmTbkItem uatmTbkItem) {
        Intent it = new Intent(act, ItemDetailActivity.class);
        it.putExtra("uatmTbkItem", uatmTbkItem);
        act.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.tbk_activity_item_detail);


        lunbo = findViewById(R.id.lunbo);
        back = findViewById(R.id.back);
        tvVolume = findViewById(R.id.tv_volume);
        tvPriceAfter = findViewById(R.id.tv_price_after);
        tvPrice = findViewById(R.id.tv_price);
        tvCoupon = findViewById(R.id.tv_coupon);
        tvTkl = findViewById(R.id.tv_tkl);
        btnCopy = findViewById(R.id.tv_copy);
        btnOpen = findViewById(R.id.tv_open);
        layoutBottom = findViewById(R.id.layout_bottom);
        tvTitle = findViewById(R.id.tv_title);
        layoutCoupon = findViewById(R.id.layout_coupon);
        tvTklTips = findViewById(R.id.tv_tkl_tips);
        layoutShare = findViewById(R.id.layout_share);
        layoutOpen = findViewById(R.id.layout_open);

        tvTklTips.setOnClickListener(this);
        tvTkl.setOnClickListener(this);
        back.setOnClickListener(this);
        layoutShare.setOnClickListener(this);
        layoutOpen.setOnClickListener(this);


        try {
            tbkCoupon = (TbkDgItemCouponGetResponse.TbkCoupon) getIntent().getSerializableExtra
                    ("tbkCoupon");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            uatmTbkItem = (UatmTbkItem) getIntent().getSerializableExtra("uatmTbkItem");
        } catch (Exception e) {
            e.printStackTrace();
        }
        isTBKCoupon = uatmTbkItem == null;
        initLunbo();
        initViews();

        //从Server制作淘口令：
        if (isTBKCoupon) {
            XMBApi.buildTKL(this, tbkCoupon.getTitle(),
                    tbkCoupon.getCouponClickUrl(), tbkCoupon.getPictUrl(), tbkCoupon
                            .getZkFinalPrice(), tbkCoupon.getCouponInfo(), tbkCoupon
                            .getItemDescription());
        } else {
            XMBApi.buildTKL(this, uatmTbkItem.getTitle(),
                    uatmTbkItem.getCouponClickUrl(), uatmTbkItem.getPictUrl(), uatmTbkItem
                            .getZkFinalPrice(), uatmTbkItem.getCouponInfo(), null);
        }

    }

    private void initViews() {
        if (isTBKCoupon) {
            tvTitle.setText(tbkCoupon.getTitle());
            double coupon = formatCouponInfo(tbkCoupon.getCouponInfo());
            String userType = tbkCoupon.getUserType() == 1L ? "天猫价￥" : "淘宝价￥";
            tvPrice.setText(userType + tbkCoupon.getZkFinalPrice());
            double priceNow = Double.parseDouble(tbkCoupon.getZkFinalPrice()) - coupon;
            tvPriceAfter.setText("￥" + subZeroAndDot(String.format("%.2f", priceNow)));
            tvVolume.setText("月销" + tbkCoupon.getVolume());
            String formatCoupon = subZeroAndDot(coupon + "");
            tvCoupon.setText(formatCoupon + "元");
            if (StringUtils.isEmpty(tbkCoupon.getCouponClickUrl())) {
                btnOpen.setText("打开宝贝");
                layoutShare.setVisibility(View.GONE);
                layoutCoupon.setVisibility(View.GONE);
            } else {
                btnOpen.setText("领券 ￥" + formatCoupon);
            }
        } else {
            tvTitle.setText(uatmTbkItem.getTitle());
            double coupon = formatCouponInfo(uatmTbkItem.getCouponInfo());
            String userType = uatmTbkItem.getUserType() == 1L ? "天猫价￥" : "淘宝价￥";
            tvPrice.setText(userType + uatmTbkItem.getZkFinalPrice());
            double priceNow = Double.parseDouble(uatmTbkItem.getZkFinalPrice()) - coupon;
            tvPriceAfter.setText("￥" + subZeroAndDot(String.format("%.2f", priceNow)));
            tvVolume.setText("月销" + uatmTbkItem.getVolume());
            String formatCoupon = subZeroAndDot(coupon + "");
            tvCoupon.setText(formatCoupon + "元");
            btnOpen.setText("领券 ￥" + formatCoupon);
            if (StringUtils.isEmpty(uatmTbkItem.getCouponClickUrl())) {
                btnOpen.setText("打开宝贝");
                layoutShare.setVisibility(View.GONE);
                layoutCoupon.setVisibility(View.GONE);
            } else {
                btnOpen.setText("领券 ￥" + formatCoupon);
            }
        }
    }

    private double formatCouponInfo(String couponInfo) {
        if (StringUtils.isEmpty(couponInfo)) return 0;
        if (couponInfo.indexOf("减") != -1 && couponInfo.lastIndexOf("元") == couponInfo.length
                () - 1) {
            couponInfo = couponInfo.substring(couponInfo.indexOf("减") + 1, couponInfo
                    .lastIndexOf("元"));
            try {
                return Double.parseDouble(couponInfo);
            } catch (NumberFormatException e) {
                return 0;
            }
        }
        return 0;
    }

    /**
     * 使用java正则表达式去掉多余的.与0
     *
     * @param s
     * @return
     */
    public String subZeroAndDot(String s) {
        if (s.indexOf(".") > 0) {
            s = s.replaceAll("0+?$", "");//去掉多余的0
            s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
        }
        return s;
    }

    private void initLunbo() {
        //动态设置图片高度为屏幕宽度：
        int screenWidth = ScreenUtils.getScreenWidth();
        //这个LayoutParams是看父容器属于哪种布局而定的
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(screenWidth,
                screenWidth);
        lunbo.setLayoutParams(params);

        lunbo.setBannerStyle(BannerConfig.NUM_INDICATOR);
        List<String> imgs;
        if (isTBKCoupon) {
            imgs = tbkCoupon.getSmallImages();
        } else {
            imgs = uatmTbkItem.getSmallImages();
        }
        if (imgs != null) {
            //设置图片加载器
            lunbo.setImageLoader(new LunboImageLoader());
            //设置图片集合
            lunbo.setImages(imgs);
            //banner设置方法全部调用完毕时最后调用
            lunbo.start();
        }
    }

    private void copyTKL() {
        BaseUtils.openCopy(this, tkl, false);
        ToastUtils.showLong("复制成功，打开“手机淘宝”即可领券");
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.back) {
            finish();

        } else if (i == R.id.layout_share) {//事件统计;
            try {
                MTA.sendEvent("分享", tkl);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.SUBJECT", "分享");
            intent.putExtra("android.intent.extra.TEXT", tkl);
            startActivity(Intent.createChooser(intent, "内部优惠券"));

        } else if (i == R.id.layout_open) {//事件统计;
            try {
                MTA.sendEvent("跳转到领券", tkl);
            } catch (Exception e) {
                e.printStackTrace();
            }

            openItem();

        } else if (i == R.id.tv_tkl_tips || i == R.id.tv_tkl) {
            if (!StringUtils.isEmpty(tkl))
                copyTKL();
        }
    }

    private void openItem() {
        //判断 淘宝App 是否安装:
        boolean isTBInstall = AppUtils.isInstallApp("com.taobao.taobao");

        String tbUrl;
        if (isTBKCoupon) {
            tbUrl = tbkCoupon.getCouponClickUrl();
        } else {
            tbUrl = uatmTbkItem.getCouponClickUrl();
        }
        if (StringUtils.isEmpty(tbUrl)) {//无券：
            if (isTBKCoupon) {
                tbUrl = tbkCoupon.getItemUrl();
            } else {
                tbUrl = uatmTbkItem.getClickUrl();
            }
        }

        //如果未安装淘宝，则直接用浏览器领券：
        if (!isTBInstall) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            Uri content_url = Uri.parse(tbUrl);
            intent.setData(content_url);
            startActivity(intent);
            return;
        }

        tbUrl = tbUrl.replace("https://", "");
        tbUrl = tbUrl.replace("http://", "");
        tbUrl = "taobao://" + tbUrl;
        Intent intent =
                new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri uri = Uri.parse(tbUrl);
        intent.setData(uri);
        startActivity(intent);


    }

    @Override
    public void onFailure(Call call, Exception e) {

    }

    @Override
    public void onResponse(final String obj, Call call, Response response) {
        if (response.isSuccessful() && !StringUtils.isEmpty(obj)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tkl = obj;
                    tvTklTips.setVisibility(View.VISIBLE);
                    tvTkl.setText(obj);
                }
            });
        }
    }


    public class LunboImageLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            //Picasso 加载图片简单用法
            Picasso.with(context).load((String) path).into(imageView);
        }

    }


}
