package com.xmb.taobaoke;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.xmb.taobaoke.bean.BaseIndexBean;
import com.xmb.taobaoke.bean.ServerConfigBean;
import com.xmb.taobaoke.util.ServerConfigDB;
import com.xmb.taobaoke.web.MTA;

import java.util.List;

public class MainActivity extends BaseAppCompatActivity {
    public static Activity act;
    private BottomNavigationBar bottomNavigationBar;
    private Fragment curFrag;
    private IndexZiXuanKuFragment indexZiXuanKuFragment;
    private ZiXuanKuFragment nineyuanFragment;
    private ZiXuanKuFragment qinggouFragment;
//    private AboutFragment aboutFragment;
    private ServerConfigBean configBean;
    private List<BaseIndexBean> tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        act = this;
        setContentView(R.layout.tbk_activity_main);
        configBean = ServerConfigDB.get();
        tabs = configBean.getTabs();
        bottomNavigationBar = (BottomNavigationBar) findViewById(R.id.bottom_navigation_bar);
        bottomNavigationBar
                .addItem(new BottomNavigationItem(R.drawable.tbk_icon_index, tabs.get(0).getTitle()))
                .addItem(new BottomNavigationItem(R.drawable.tbk_icon_qianggou, tabs.get(1).getTitle()))
                .addItem(new BottomNavigationItem(R.drawable.tbk_icon_99, tabs.get(2).getTitle()))
//                .addItem(new BottomNavigationItem(R.drawable.tbk_icon_about, "关于我们"))
                .initialise();
        bottomNavigationBar.setTabSelectedListener(new BottomNavigationBar.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position) {
                changeFrag(position);
            }

            @Override
            public void onTabUnselected(int position) {
            }

            @Override
            public void onTabReselected(int position) {
            }
        });

        bottomNavigationBar.selectTab(0);

        //事件统计;
        try {
            MTA.sendEvent("启动","");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
       finish();
    }

    private void changeFrag(int position) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        // 先隐藏掉所有的Fragment，以防止有多个Fragment显示在界面上的情况
        if (curFrag != null) {
            transaction.hide(curFrag);
        }
        switch (position) {
            case 0:
                if (indexZiXuanKuFragment == null) {
                    indexZiXuanKuFragment = IndexZiXuanKuFragment.newInstance(tabs.get(0).getData
                            ());
                    transaction.add(R.id.frag_container, indexZiXuanKuFragment);
                } else {
                    transaction.show(indexZiXuanKuFragment);
                }
                curFrag = indexZiXuanKuFragment;
                break;
            case 1:
                if (qinggouFragment == null) {
                    qinggouFragment = ZiXuanKuFragment.newInstance(tabs.get(1).getData());
                    transaction.add(R.id.frag_container, qinggouFragment);
                } else {
                    transaction.show(qinggouFragment);
                }
                curFrag = qinggouFragment;
                break;
            case 2:
                if (nineyuanFragment == null) {
                    nineyuanFragment = ZiXuanKuFragment.newInstance(tabs.get(2).getData());
                    transaction.add(R.id.frag_container, nineyuanFragment);
                } else {
                    transaction.show(nineyuanFragment);
                }
                curFrag = nineyuanFragment;
                break;
//            case 3:
//                if (aboutFragment == null) {
//                    aboutFragment = new AboutFragment();
//                    transaction.add(R.id.frag_container, aboutFragment);
//                } else {
//                    transaction.show(aboutFragment);
//                }
//                curFrag = aboutFragment;
//                break;
        }
        transaction.commitAllowingStateLoss();
    }

}
