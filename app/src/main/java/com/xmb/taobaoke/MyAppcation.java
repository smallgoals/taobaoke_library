package com.xmb.taobaoke;

public class MyAppcation extends com.nil.crash.utils.CrashApp {
    public static String CHANNEL = "XMB";

    @Override
    public void onCreate() {
        super.onCreate();

        //代理渠道标志，配置在XMB里：
        CHANNEL = getString(R.string.proxy_channel);
    }
}
