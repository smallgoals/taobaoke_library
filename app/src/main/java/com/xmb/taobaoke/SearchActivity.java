package com.xmb.taobaoke;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.StringUtils;
import com.google.android.flexbox.FlexboxLayout;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.utils.KeyBoardUtils;
import com.xmb.taobaoke.util.KeywordHistoryDB;

import java.util.ArrayList;

public class SearchActivity extends BaseAppCompatActivity implements TextView
        .OnEditorActionListener, TextWatcher, View.OnClickListener {

    ImageView ivBack;
    EditText etKeyword;
    ImageView ivReset;
    Button btnSearch;
    FlexboxLayout containerKeyword;
    ImageView cleanHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.tbk_activity_search);

        ivBack = findViewById(R.id.iv_back);
        etKeyword = findViewById(R.id.et_keyword);
        ivReset = findViewById(R.id.iv_reset);
        btnSearch = findViewById(R.id.btn_search);
        containerKeyword = findViewById(R.id.container_keyword);
        cleanHistory = findViewById(R.id.clean_history);

        etKeyword.setOnEditorActionListener(this);
        etKeyword.addTextChangedListener(this);

        ivBack.setOnClickListener(this);
        ivReset.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        cleanHistory.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateKeywordHistory();
//        InputUtil.showInputMethod(this, etKeyword);
        KeyBoardUtils.openKeybord(etKeyword, this);
    }

    private void updateKeywordHistory() {
        ArrayList<String> list = KeywordHistoryDB.get();
        containerKeyword.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(this);
        if (list == null) return;
        for (int i = list.size(); i > 0; i--) {
            View view = inflater.inflate(R.layout.tbk_item_keyword, null);
            TextView tv = view.findViewById(R.id.tv_keyword);
            final String key = list.get(i - 1);
            tv.setText(key);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    etKeyword.setText(key);
                    doSearch();
                }
            });
            containerKeyword.addView(view);
        }
    }


    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.iv_back) {
            finish();

        } else if (i == R.id.iv_reset) {
            etKeyword.setText("");

        } else if (i == R.id.btn_search) {
            doSearch();

        } else if (i == R.id.clean_history) {
            KeywordHistoryDB.clean();
            containerKeyword.removeAllViews();

        }
    }

    private void doSearch() {
        String keyword = etKeyword.getText().toString().trim();
        if (StringUtils.isEmpty(keyword)) return;
        KeyBoardUtils.closeKeybord(etKeyword, this);
        SearchResultActivity.start(getActivity(), keyword);
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        doSearch();
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (StringUtils.isEmpty(etKeyword.getText())) {
            ivReset.setVisibility(View.INVISIBLE);
        } else {
            ivReset.setVisibility(View.VISIBLE);
        }
    }

}
