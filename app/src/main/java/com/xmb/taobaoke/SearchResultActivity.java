package com.xmb.taobaoke;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.utils.KeyBoardUtils;
import com.squareup.picasso.Picasso;
import com.taobao.api.response.TbkDgItemCouponGetResponse;
import com.xmb.taobaoke.util.KeywordHistoryDB;
import com.xmb.taobaoke.web.XMBApi;
import com.xmb.taobaoke.web.XMBApiCallback;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

public class SearchResultActivity extends BaseAppCompatActivity implements TextView
        .OnEditorActionListener, TextWatcher, XMBApiCallback<TbkDgItemCouponGetResponse>, View
        .OnClickListener {

    ImageView ivBack;
    EditText etKeyword;
    ImageView ivReset;
    Button btnSearch;
    RecyclerView rv;
    TextView noResultTips;
    ImageView btnTop;
    SpinKitView loading;
    private TbkDgItemCouponGetResponse tbkDgItemCouponGetResponse;
    private List<TbkDgItemCouponGetResponse.TbkCoupon> list = new
            ArrayList<TbkDgItemCouponGetResponse.TbkCoupon>();
    private MyAdapter adapter;
    private String keyword;
    private int pageSize = 100;
    private int pageNo = 1;

    public static void start(Activity act, String keyword) {
        Intent it = new Intent(act, SearchResultActivity.class);
        it.putExtra("keyword", keyword);
        act.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.tbk_activity_search_result);

        ivBack = findViewById(R.id.iv_back);
        etKeyword = findViewById(R.id.et_keyword);
        ivReset = findViewById(R.id.iv_reset);
        btnSearch = findViewById(R.id.btn_search);
        rv = findViewById(R.id.rv);
        noResultTips = findViewById(R.id.no_result_tips);
        btnTop = findViewById(R.id.btn_top);
        loading = findViewById(R.id.loading);

        ivBack.setOnClickListener(this);
        ivReset.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnTop.setOnClickListener(this);

        adapter = new MyAdapter();
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        //设置布局管理器
        rv.setLayoutManager(layoutManager);
        //设置为垂直布局，这也是默认的
        layoutManager.setOrientation(OrientationHelper.VERTICAL);
        //设置Adapter
        rv.setAdapter(adapter);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setHasFixedSize(true);//固定大小,提高性能
        //设置滑动到哪个位置了的监听
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position <= 10) {
                    btnTop.setVisibility(View.GONE);
                } else {
                    btnTop.setVisibility(View.VISIBLE);
                }
                return 1;//只能返回1
            }
        });


        etKeyword.setOnEditorActionListener(this);
        etKeyword.addTextChangedListener(this);

        keyword = getIntent().getStringExtra("keyword");
        etKeyword.setText(keyword);
        doSearchFromAPI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        KeyBoardUtils.closeKeybord(etKeyword, this);
    }


    private void doSearchFromAPI() {
        KeyBoardUtils.closeKeybord(etKeyword, this);
        keyword = etKeyword.getText().toString().trim();
        KeywordHistoryDB.save(keyword);
        if (StringUtils.isEmpty(keyword)) return;
        loading.setVisibility(View.VISIBLE);
        XMBApi.queryCoupon(this, keyword, pageSize + "", pageNo + "");
    }


    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.iv_back) {
            finish();

        } else if (i == R.id.iv_reset) {
            etKeyword.setText("");

        } else if (i == R.id.btn_search) {
            doSearchFromAPI();

        } else if (i == R.id.btn_top) {
            rv.scrollToPosition(0);

        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        doSearchFromAPI();
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (StringUtils.isEmpty(etKeyword.getText())) {
            ivReset.setVisibility(View.INVISIBLE);
        } else {
            ivReset.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFailure(Call call, Exception e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loading.setVisibility(View.GONE);
                ToastUtils.showLong("加载失败，请重试");
            }
        });

    }

    @Override
    public void onResponse(final TbkDgItemCouponGetResponse obj, Call call, Response response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tbkDgItemCouponGetResponse = obj;
                list = tbkDgItemCouponGetResponse.getResults();
                if (list == null || list.size() == 0) {
                    noResultTips.setVisibility(View.VISIBLE);
                } else {
                    noResultTips.setVisibility(View.INVISIBLE);
                }
                adapter.notifyDataSetChanged();
                rv.scrollToPosition(0);
                loading.setVisibility(View.GONE);
            }
        });
    }


    private class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder> {

        public MyAdapter() {
        }

        //OnCreateViewHolder用来给rv创建缓存的
        public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //参数3：判断条件 true  1.是打气 2.添加到parent
            View view = LayoutInflater.from(SearchResultActivity.this).inflate(R.layout
                    .tbk_index_recycle_view_item, parent, false);
            MyHolder holder = new MyHolder(view);
            return holder;
        }

        //给缓存控件设置数据
        public void onBindViewHolder(MyHolder holder, int position) {
            final TbkDgItemCouponGetResponse.TbkCoupon item = list.get(position);
            Picasso.with(SearchResultActivity.this)
                    .load(item.getPictUrl())
                    .placeholder(R.drawable.tbk_item_default)
                    .into(holder.iv_pic);
            holder.tv_title.setText(item.getTitle());
            holder.tv_price.setText("现价￥" + item.getZkFinalPrice());
            double coupon = formatCouponInfo(item.getCouponInfo());
            holder.tv_coupon.setText(subZeroAndDot(coupon + "") + "元");
            double priceNow = Double.parseDouble(item.getZkFinalPrice()) - coupon;
            holder.tv_price_after.setText("￥" + subZeroAndDot(String.format("%.2f", priceNow)));
            holder.tv_volume.setText("月销 " + item.getVolume());

            //如果没券：
            boolean hasCoupon = !StringUtils.isEmpty(item.getCouponClickUrl());
            holder.tv_price.setVisibility(hasCoupon ? View.VISIBLE : View.GONE);
            holder.layout_coupon.setVisibility(hasCoupon ? View.VISIBLE : View.GONE);
            holder.tv_quanhoujia.setVisibility(hasCoupon ? View.VISIBLE : View.GONE);

            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    String tbUrl;
//                    if (StringUtils.isEmpty(item.getCouponClickUrl())) {//无券：
//                        tbUrl = item.getItemUrl();
//                    } else {//有券：
//                        tbUrl = item.getCouponClickUrl();
//                    }
//                    tbUrl = tbUrl.replace("https://", "");
//                    tbUrl = tbUrl.replace("http://", "");
//                    tbUrl = "taobao://" + tbUrl;
//                    Intent intent =
//                            new Intent();
//                    intent.setAction("android.intent.action.VIEW");
//                    Uri uri = Uri.parse(tbUrl);
//                    intent.setData(uri);
//                    startActivity(intent);
                    ItemDetailActivity.start(SearchResultActivity.this, item);
                }
            });
        }

        private double formatCouponInfo(String couponInfo) {
            if (StringUtils.isEmpty(couponInfo)) return 0;
            if (couponInfo.indexOf("减") != -1 && couponInfo.lastIndexOf("元") == couponInfo.length
                    () - 1) {
                couponInfo = couponInfo.substring(couponInfo.indexOf("减") + 1, couponInfo
                        .lastIndexOf("元"));
                try {
                    return Double.parseDouble(couponInfo);
                } catch (NumberFormatException e) {
                    return 0;
                }
            }
            return 0;
        }

        /**
         * 使用java正则表达式去掉多余的.与0
         *
         * @param s
         * @return
         */
        public String subZeroAndDot(String s) {
            if (s.indexOf(".") > 0) {
                s = s.replaceAll("0+?$", "");//去掉多余的0
                s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
            }
            return s;
        }

        //获取记录数
        public int getItemCount() {
            if (list == null) return 0;
            return list.size();
        }

        public class MyHolder extends RecyclerView.ViewHolder {
            public ViewGroup layout;
            public ViewGroup layout_coupon;
            public ImageView iv_pic;
            public TextView tv_title, tv_price, tv_coupon, tv_price_after, tv_volume, tv_quanhoujia;

            //实现的方法
            public MyHolder(View itemView) {
                super(itemView);
                layout = itemView.findViewById(R.id.layout);
                layout_coupon = itemView.findViewById(R.id.layout_coupon);
                iv_pic = (ImageView) itemView.findViewById(R.id.iv_pic);
                tv_title = (TextView) itemView.findViewById(R.id.tv_title);
                tv_price = (TextView) itemView.findViewById(R.id.tv_price);
                tv_coupon = (TextView) itemView.findViewById(R.id.tv_coupon);
                tv_price_after = (TextView) itemView.findViewById(R.id.tv_price_after);
                tv_volume = (TextView) itemView.findViewById(R.id.tv_volume);
                tv_quanhoujia = (TextView) itemView.findViewById(R.id.tv_quanhoujia);
            }
        }
    }
}
