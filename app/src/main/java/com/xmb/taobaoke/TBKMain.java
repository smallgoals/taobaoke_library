package com.xmb.taobaoke;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;

import com.xmb.taobaoke.bean.ServerConfigBean;
import com.xmb.taobaoke.web.XMBApi;
import com.xmb.taobaoke.web.XMBApiCallback;

public class TBKMain {

    /**
     * 打开淘宝客Activity
     *
     * @param act
     */
    public static void start4Activity(Activity act) {
        Intent it = new Intent(act, com.xmb.taobaoke.WelcomeActivity.class);
        act.startActivity(it);
    }


    public static TBKIndexFragment start4Fragment() {
        TBKIndexFragment indexZiXuanKuFragment = TBKIndexFragment.newInstance
                ();
        return indexZiXuanKuFragment;
    }

}
