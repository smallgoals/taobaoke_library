package com.xmb.taobaoke;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.nil.sdk.ui.BaseAppCompatActivity;


/**
 * Created by dengz on 2017/11/3.
 */

public class WebHtmlActivity extends BaseAppCompatActivity {
    private WebView webView;
    private String url, title;

    public static void start(Context ctx, String title, String url) {
        Intent it = new Intent(ctx, WebHtmlActivity.class);
        it.putExtra("title", title);
        it.putExtra("url", url);
        ctx.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tbk_activity_web_html);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        title = getIntent().getStringExtra("title");
        setTitle(title);
        url = getIntent().getStringExtra("url");
        webView = findViewById(R.id.webView);
        webView.loadUrl(url);
        //覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
                view.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
