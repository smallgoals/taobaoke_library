package com.xmb.taobaoke;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;

import com.blankj.utilcode.util.ActivityUtils;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.xmb.taobaoke.bean.ServerConfigBean;
import com.xmb.taobaoke.web.XMBApi;
import com.xmb.taobaoke.web.XMBApiCallback;

import okhttp3.Call;
import okhttp3.Response;

public class WelcomeActivity extends BaseAppCompatActivity implements
        XMBApiCallback<ServerConfigBean> {
    // 欢迎界面启动时的最少延时（1.5秒）：
    private final long INTERVAL_TIME = (long) (0.5 * 1000);
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.tbk_activity_welcome);
        load();
    }

    //开始时间：
    long beginTime;

    private void load() {
        //开始时间：
        if (handler == null) handler = new Handler();
        beginTime = System.currentTimeMillis();
        XMBApi.loadServerConfig(this);
    }

    private void showFailure() {
        new AlertDialog.Builder(this).setMessage("网络出了故障，请检查网络后重试").setCancelable(false)
                .setNegativeButton("退出",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        })
                .setPositiveButton("重试", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        load();
                    }
                }).show();
    }

    @Override
    public void onFailure(Call call, Exception e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showFailure();
            }
        });
    }

    @Override
    public void onResponse(ServerConfigBean obj, Call call, Response response) {
        long endTim = System.currentTimeMillis();
        long interval = endTim - beginTime;
        if (interval < INTERVAL_TIME) {
            interval = INTERVAL_TIME - interval;
        } else {
            interval = 0;
        }

        //延时启动：
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ActivityUtils.startActivity(MainActivity.class);
                finish();
            }
        }, interval);
    }
}
