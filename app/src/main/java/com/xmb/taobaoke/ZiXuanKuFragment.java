package com.xmb.taobaoke;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.nil.sdk.nb.utils.NbDensityUtils;
import com.nil.sdk.ui.BaseFragmentV4;
import com.squareup.picasso.Picasso;
import com.taobao.api.domain.UatmTbkItem;
import com.taobao.api.response.TbkUatmFavoritesItemGetResponse;
import com.xmb.taobaoke.bean.ServerConfigBean;
import com.xmb.taobaoke.util.ServerConfigDB;
import com.xmb.taobaoke.web.XMBApi;
import com.xmb.taobaoke.web.XMBApiCallback;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

public class ZiXuanKuFragment extends BaseFragmentV4 implements
        XMBApiCallback<TbkUatmFavoritesItemGetResponse>, View.OnClickListener {
    RecyclerView rv;
    ImageView btnTop;
    SpinKitView loading;
    private TbkUatmFavoritesItemGetResponse tbkUatmFavoritesItemGetResponse;
    private List<UatmTbkItem> list = new ArrayList<UatmTbkItem>();
    private MyAdapter myAdapter;
    private int screenWidth;
    private int ivHeightPx;
    private int itemMarginPx;
    private ServerConfigBean configBean;

    public static ZiXuanKuFragment newInstance(String favoritesType) {
        ZiXuanKuFragment f = new ZiXuanKuFragment();
        Bundle b = new Bundle();
        b.putCharSequence("favoritesType", favoritesType);
        f.setArguments(b);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tbk_fragment_zixuanku, container, false);

        rv = rootView.findViewById(R.id.rv);
        btnTop = rootView.findViewById(R.id.btn_top);
        loading = rootView.findViewById(R.id.loading);
        btnTop.setOnClickListener(this);

        configBean = ServerConfigDB.get();

        //为轮播图片长宽：
        screenWidth = ScreenUtils.getScreenWidth();
        ivHeightPx = NbDensityUtils.dp2px(getActivity(), 190);
        itemMarginPx = NbDensityUtils.dp2px(getActivity(), 4);

        myAdapter = new MyAdapter(getContext(), R.layout.tbk_index_recycle_view_item, list);

        final GridLayoutManager layoutManager = new GridLayoutManager(this.getContext(), 2);
        //设置布局管理器
        rv.setLayoutManager(layoutManager);
        //设置为垂直布局，这也是默认的
        layoutManager.setOrientation(OrientationHelper.VERTICAL);
        //设置Adapter
        rv.setAdapter(myAdapter);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setHasFixedSize(true);//固定大小,提高性能
        //设置滑动到哪个位置了的监听
        final int height = ScreenUtils.getScreenHeight();
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int adapterNowPos = layoutManager.findFirstVisibleItemPosition();
                if (adapterNowPos <= 10) {
                    btnTop.setVisibility(View.GONE);
                } else {
                    btnTop.setVisibility(View.VISIBLE);
                }
            }
        });
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (tbkUatmFavoritesItemGetResponse == null) {
            String favoritesType = getArguments().getCharSequence("favoritesType").toString();
            XMBApi.zixuanku(this, favoritesType);
        }
    }


    @Override
    public void onFailure(Call call, final Exception e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loading.setVisibility(View.GONE);
                ToastUtils.showLong("加载失败，请重试");
            }
        });

    }

    @Override
    public void onResponse(final TbkUatmFavoritesItemGetResponse obj, Call call, Response
            response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tbkUatmFavoritesItemGetResponse = obj;
                list = tbkUatmFavoritesItemGetResponse.getResults();
                if (list != null) {
                    myAdapter.getDatas().clear();
                    myAdapter.getDatas().addAll(list);
                    myAdapter.notifyDataSetChanged();
                }
                loading.setVisibility(View.GONE);
            }
        });
    }

    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btn_top) {
            rv.scrollToPosition(0);

        }
    }

    private class MyAdapter extends CommonAdapter<UatmTbkItem> {


        public MyAdapter(Context context, int layoutId, List datas) {
            super(context, layoutId, datas);
        }

        @Override
        protected void convert(ViewHolder holder, final UatmTbkItem item, int position) {
            Picasso.with(getContext())
                    .load(item.getPictUrl())
                    .placeholder(R.drawable.tbk_item_default)
                    .into((ImageView) holder.getView(R.id.iv_pic));
            holder.setText(R.id.tv_title, item.getTitle());
            holder.setText(R.id.tv_price, "现价￥" + item.getZkFinalPrice());
            double coupon = formatCouponInfo(item.getCouponInfo());
            holder.setText(R.id.tv_coupon, subZeroAndDot(coupon + "") + "元");
            double priceNow = Double.parseDouble(item.getZkFinalPrice()) - coupon;
            holder.setText(R.id.tv_price_after, "￥" + subZeroAndDot(String.format("%.2f",
                    priceNow)));
            holder.setText(R.id.tv_volume, "月销 " + item.getVolume());

            //如果没券：
            boolean hasCoupon = !StringUtils.isEmpty(item.getCouponClickUrl());
            holder.setVisible(R.id.tv_price, hasCoupon);
            holder.setVisible(R.id.layout_coupon, hasCoupon);
            holder.setVisible(R.id.tv_quanhoujia, hasCoupon);

            holder.setOnClickListener(R.id.layout, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ItemDetailActivity.start(getActivity(), item);
                }
            });

            RelativeLayout layout = holder.getView(R.id.layout);
            if (position % 2 == 0) {
                layout.setPadding(itemMarginPx / 2, 0, itemMarginPx, itemMarginPx);
            } else {
                layout.setPadding(itemMarginPx, 0, itemMarginPx / 2, itemMarginPx);
            }
        }

        private double formatCouponInfo(String couponInfo) {
            if (StringUtils.isEmpty(couponInfo)) return 0;
            if (couponInfo.indexOf("减") != -1 && couponInfo.lastIndexOf("元") == couponInfo.length
                    () - 1) {
                couponInfo = couponInfo.substring(couponInfo.indexOf("减") + 1, couponInfo
                        .lastIndexOf("元"));
                try {
                    return Double.parseDouble(couponInfo);
                } catch (NumberFormatException e) {
                    return 0;
                }
            }
            return 0;
        }

        /**
         * 使用java正则表达式去掉多余的.与0
         *
         * @param s
         * @return
         */
        public String subZeroAndDot(String s) {
            if (s.indexOf(".") > 0) {
                s = s.replaceAll("0+?$", "");//去掉多余的0
                s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
            }
            return s;
        }
    }

}
