package com.xmb.taobaoke.bean;

import java.io.Serializable;

public class BaseIndexBean implements Serializable {
    public static final String TYPE_ZIXUANKU = "zixuanku";
    public static final String TYPE_URL2MYSELF = "url2myself";
    public static final String TYPE_URL2TAOBAO = "url2taobao";

    private String imgUrl;
    private String type;
    private String data;
    private String des;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
}
