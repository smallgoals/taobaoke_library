package com.xmb.taobaoke.bean;

import java.io.Serializable;
import java.util.List;

public class ServerConfigBean implements Serializable {

    private List<BaseIndexBean> tabs;
    private List<BaseIndexBean> index_lunbo;
    private List<BaseIndexBean> index_category;

    public List<BaseIndexBean> getTabs() {
        return tabs;
    }

    public void setTabs(List<BaseIndexBean> tabs) {
        this.tabs = tabs;
    }

    public List<BaseIndexBean> getIndex_lunbo() {
        return index_lunbo;
    }

    public void setIndex_lunbo(List<BaseIndexBean> index_lunbo) {
        this.index_lunbo = index_lunbo;
    }

    public List<BaseIndexBean> getIndex_category() {
        return index_category;
    }

    public void setIndex_category(List<BaseIndexBean> index_category) {
        this.index_category = index_category;
    }
}
