package com.xmb.taobaoke.util;

import com.nil.crash.utils.CrashApp;

import java.util.ArrayList;

public class KeywordHistoryDB {
    private static final String TAG = "KeywordHistoryDB";


    public static void clean() {
        ACache.get(CrashApp.CONTEXT).remove(TAG);
    }

    public static void save(String keyword) {
        ArrayList<String> list = (ArrayList<String>) ACache.get(CrashApp.CONTEXT).getAsObject(TAG);
        if (list == null) list = new ArrayList<>();

        //如果有相同记录，则把老记录删除：
        for (String str : list) {
            if (str.equals(keyword)) {
                list.remove(str);
                break;
            }
        }
        list.add(keyword);

        if (list.size() > 20) {
            list.remove(0);
        }

        ACache.get(CrashApp.CONTEXT).put(TAG, list);
    }


    public static ArrayList<String> get() {
        return (ArrayList<String>) ACache.get(CrashApp.CONTEXT).getAsObject(TAG);
    }

}
