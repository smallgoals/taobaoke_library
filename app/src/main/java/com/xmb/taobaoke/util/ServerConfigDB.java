package com.xmb.taobaoke.util;

import com.nil.crash.utils.CrashApp;
import com.xmb.taobaoke.bean.ServerConfigBean;

public class ServerConfigDB {

    public static void delete() {
        ACache a = ACache.get(CrashApp.CONTEXT);
        a.remove("ServerConfigBean");
    }

    public static void save(ServerConfigBean appConfigBean) {
        ACache a = ACache.get(CrashApp.CONTEXT);
        a.put("ServerConfigBean", appConfigBean);
    }

    public static ServerConfigBean get() {
        ACache a = ACache.get(CrashApp.CONTEXT);
        ServerConfigBean bean = (ServerConfigBean) a.getAsObject("ServerConfigBean");
        return bean;
    }


}
