package com.xmb.taobaoke.web;

import java.util.HashMap;
import java.util.Map;

public class MTA {
    private static final String app_id = "taobaoke";

    public static void sendEvent(Map<String, String> map) {
        //事件统计;
        try {
            map.put("app_id", app_id);
            com.xmb.mta.util.XMBApi.sendEvent(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendEvent(String event_name, String content) {
        try {
            Map<String, String> map = new HashMap<>();
            map.put("app_id", app_id);
            map.put("event_name", event_name);
            map.put("content", content);
//            map.put("type", "综合");
            com.xmb.mta.util.XMBApi.sendEvent(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
