package com.xmb.taobaoke.web;


import com.taobao.api.domain.UatmTbkItem;
import com.taobao.api.response.TbkDgItemCouponGetResponse;
import com.taobao.api.response.TbkUatmFavoritesItemGetResponse;
import com.xmb.mta.util.XMBGson;
import com.xmb.mta.util.XMBOkHttp;
import com.xmb.taobaoke.MyAppcation;
import com.xmb.taobaoke.bean.ServerConfigBean;
import com.xmb.taobaoke.util.ServerConfigDB;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


public class XMBApi {
    private static final String URL_DATA_HEAD = "http://goodapp.vip/taobaoke/";
//    private static final String CHANNEL = "XMB";
//    public static final String FAVORITESTYPE_INDEX = "INDEX";
//    public static final String FAVORITESTYPE_QIANGGOU = "QIANGGOU";
//    public static final String FAVORITESTYPE_9KUAI9 = "9KUAI9";


    /**
     * 加载Server上的配置
     */
    public static void loadServerConfig(final XMBApiCallback<ServerConfigBean>
                                                xmbApiCallback) {
        String url = "http://xiaomubiaokeji.com/taobaoke/Server_Json_Config.json";
        XMBOkHttp.doGet(url, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String data = response.body().string();
                    ServerConfigBean result = XMBGson.getGson().fromJson(data,
                            ServerConfigBean.class);
                    if (result != null) {
                        ServerConfigDB.save(result);
                    }
                    if (xmbApiCallback != null) xmbApiCallback.onResponse(result, call, response);
                } catch (Exception e) {
                    e.printStackTrace();
                    xmbApiCallback.onFailure(call, e);
                }
            }
        });
    }

    /**
     * 制作淘口令
     */
    public static void buildTKL(final XMBApiCallback<String>
                                        xmbApiCallback, String title,
                                String couponClickUrl, String picUrl, String zkFinalPrice, String
                                        couponInfo, String itemDes) {
        String apiName = "OpenCoupon";
        Map<String, String> map = new HashMap<String, String>();
        map.put("channel", MyAppcation.CHANNEL);
        map.put("title", title);
        map.put("couponClickUrl", couponClickUrl);
        map.put("picUrl", picUrl);
        map.put("zkFinalPrice", zkFinalPrice);
        map.put("couponInfo", couponInfo);
        if (itemDes != null)
            map.put("itemDes", itemDes);
        String data = map2KeyValue(map);

        XMBOkHttp.doGet(URL_DATA_HEAD + apiName + "?" + data, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String data = response.body().string();
                    if (xmbApiCallback != null) xmbApiCallback.onResponse(data, call, response);
                } catch (Exception e) {
                    e.printStackTrace();
                    xmbApiCallback.onFailure(call, e);
                }
            }
        });
    }


    /**
     * 选品库
     *
     * @param xmbApiCallback
     * @param favoritesType
     */
    public static void zixuanku(final XMBApiCallback<TbkUatmFavoritesItemGetResponse>
                                        xmbApiCallback, String favoritesType) {
        String apiName = "ZiXuanKu";
        Map<String, String> map = new HashMap<String, String>();
        map.put("channel", MyAppcation.CHANNEL);
        map.put("favoritesType", favoritesType);
        String data = map2KeyValue(map);

        XMBOkHttp.doGet(URL_DATA_HEAD + apiName + "?" + data, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String data = response.body().string();
                    TbkUatmFavoritesItemGetResponse result = XMBGson.getGson().fromJson(data,
                            TbkUatmFavoritesItemGetResponse.class);

                    //做乱序处理：
                    if (result != null) {
                        List<UatmTbkItem> list = result.getResults();
                        if (list != null)
                            Collections.shuffle(list);
                    }


                    if (xmbApiCallback != null) xmbApiCallback.onResponse(result, call, response);
                } catch (Exception e) {
                    e.printStackTrace();
                    xmbApiCallback.onFailure(call, e);
                }
            }
        });
    }

    /**
     * 搜索
     *
     * @param xmbApiCallback
     * @param q
     * @param pageSize
     * @param pageNo
     */
    public static void queryCoupon(final XMBApiCallback<TbkDgItemCouponGetResponse>
                                           xmbApiCallback, String q, String pageSize, String
                                           pageNo) {
        //事件统计;
        try {
            MTA.sendEvent("搜券", q);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String apiName = "QueryCoupon";
        Map<String, String> map = new HashMap<String, String>();
        map.put("channel", MyAppcation.CHANNEL);
        map.put("q", q);
        map.put("pageSize", pageSize);
        map.put("pageNo", pageNo);
        String data = map2KeyValue(map);

        XMBOkHttp.doGet(URL_DATA_HEAD + apiName + "?" + data, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String data = response.body().string();
                    TbkDgItemCouponGetResponse result = XMBGson.getGson().fromJson(data,
                            TbkDgItemCouponGetResponse.class);
                    if (xmbApiCallback != null) xmbApiCallback.onResponse(result, call, response);
                } catch (Exception e) {
                    e.printStackTrace();
                    xmbApiCallback.onFailure(call, e);
                }
            }
        });
    }


    private static String map2KeyValue(Map<String, String> map) {
        String str = "";
        for (Map.Entry<String, String> entry : map.entrySet()) {
            str += "&" + entry.getKey() + "=" + entry.getValue();
        }
        str = str.replaceFirst("&", "");
        return str;
    }


}
