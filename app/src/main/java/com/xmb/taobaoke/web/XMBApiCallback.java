package com.xmb.taobaoke.web;

import java.io.IOException;
import java.io.Serializable;

import okhttp3.Call;
import okhttp3.Response;

public interface XMBApiCallback<T extends Serializable> {

    void onFailure(Call call, Exception e);

    void onResponse(T obj, Call call, Response response) ;
}
