package com.xmb.taobaoke.web;

import com.blankj.utilcode.util.NetworkUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class XMBOkHttp {
    private static OkHttpClient okHttpClient = null;


    private XMBOkHttp() {
    }

    public static OkHttpClient getInstance() {
        if (okHttpClient == null) {
            //加同步安全
            synchronized (XMBOkHttp.class) {
                if (okHttpClient == null) {
//                    File sdcache = new File(Environment.getExternalStorageDirectory(),
// "cache-2ok");
                    int cacheSize = 10 * 1024 * 1024;
                    OkHttpClient.Builder builder = new OkHttpClient.Builder();
                    builder.connectTimeout(15, TimeUnit.SECONDS);
                    builder.writeTimeout(20, TimeUnit.SECONDS).readTimeout(20, TimeUnit.SECONDS);
                    builder.addNetworkInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request request = chain.request();
                            Response response = chain.proceed(request);
                            if (NetworkUtils.isConnected()) {
                                int maxAge = 0;                     // 有网络时 设置缓存超时时间0个小时
                                response.newBuilder()
                                        .header("Cache-Control", "public, max-age=" +
                                                maxAge)
                                        .removeHeader("Pragma")     // 清除头信息
                                        .build();
                            } else {
                                int maxStale = 60 * 60 * 24;        // 无网络时，设置超时为1天
                                response.newBuilder()
                                        .header("Cache-Control", "public, only-if-cached," +
                                                " max-stale=" + maxStale)
                                        .removeHeader("Pragma")
                                        .build();
                            }
                            return response;
                        }
                    });
                    okHttpClient = builder.build();
                }
            }
        }
        return okHttpClient;
    }

    /**
     * 【请求不到，返回失败】
     * 如果没有缓存，进行get请求获取服务器数据并缓存起来
     * 如果缓存已经存在：不超过maxAge---->不进行请求,直接返回缓存数据
     * 超出了maxAge--->返回失败
     */
    public static void doGetAndCacheInAgeTime(String url, int cache_maxAge_inSeconds,
                                              Callback callback) {
        //创建OkHttpClient请求对象
        OkHttpClient okHttpClient = getInstance();
        //创建Request
        Request request = new Request.Builder()
                .cacheControl(new CacheControl.Builder().maxAge(cache_maxAge_inSeconds,
                        TimeUnit.SECONDS)
                        .build())
                .url(url).build();
        //得到Call对象
        Call call = okHttpClient.newCall(request);
        //执行异步请求
        call.enqueue(callback);
    }

    /**
     * 【请求不到，返回缓存】
     * 如果没有缓存，进行get请求获取服务器数据并缓存起来
     * 如果缓存已经存在：不超过maxStale---->不进行请求,直接返回缓存数据
     * 超出了maxStale--->可以使用过期缓存
     */
    public static void doGetAndCacheInStaleTime(String url, int cache_maxStale_inSeconds,
                                                Callback callback) {
        //创建OkHttpClient请求对象
        OkHttpClient okHttpClient = getInstance();
        //创建Request
        Request request = new Request.Builder()
                .cacheControl(new CacheControl.Builder().maxStale(cache_maxStale_inSeconds,
                        TimeUnit.SECONDS)
                        .build())
                .url(url).build();
        //得到Call对象
        Call call = okHttpClient.newCall(request);
        //执行异步请求
        call.enqueue(callback);
    }

    /**
     * get请求
     * 参数1 url
     * 参数2 回调Callback
     */

    public static void doGet(String url, Callback callback) {
        //创建OkHttpClient请求对象
        OkHttpClient okHttpClient = getInstance();
        //创建Request
        Request request = new Request.Builder().url(url).build();
        //得到Call对象
        Call call = okHttpClient.newCall(request);
        //执行异步请求
        call.enqueue(callback);
    }

    /**
     * post请求
     * 参数1 url
     * 参数2 回调Callback
     */

    public static void doPost(String url, Map<String, String> params, Callback callback) {
        //创建OkHttpClient请求对象
        OkHttpClient okHttpClient = getInstance();
        //3.x版本post请求换成FormBody 封装键值对参数

        FormBody.Builder builder = new FormBody.Builder();
        //遍历集合
        for (String key : params.keySet()) {
            builder.add(key, params.get(key));

        }
        //创建Request
        Request request = new Request.Builder().url(url).post(builder.build()).build();
    }

    /**
     * post请求上传文件
     * 参数1 url
     * 参数2 回调Callback
     */
    public static void uploadPic(String url, File file, String fileName) {
        //创建OkHttpClient请求对象
        OkHttpClient okHttpClient = getInstance();
        //创建RequestBody 封装file参数
        RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"),
                file);
        //创建RequestBody 设置类型等
        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("file", fileName, fileBody).build();
        //创建Request
        Request request = new Request.Builder().url(url).post(requestBody).build();

        //得到Call
        Call call = okHttpClient.newCall(request);
        //执行请求
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                //上传成功回调 目前不需要处理
            }
        });

    }

    /**
     * Post请求发送JSON数据
     * 参数一：请求Url
     * 参数二：请求的JSON
     * 参数三：请求回调
     */
    public static void doPostJson(String url, String jsonParams, Callback callback) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; " +
                "charset=utf-8"), jsonParams);
        Request request = new Request.Builder().url(url).post(requestBody).build();
        Call call = getInstance().newCall(request);
        call.enqueue(callback);
    }


}
