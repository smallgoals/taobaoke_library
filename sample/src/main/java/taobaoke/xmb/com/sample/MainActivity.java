package taobaoke.xmb.com.sample;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.blankj.utilcode.util.ToastUtils;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.xmb.taobaoke.IndexZiXuanKuFragment;
import com.xmb.taobaoke.TBKIndexFragment;
import com.xmb.taobaoke.TBKMain;
import com.xmb.taobaoke.bean.ServerConfigBean;
import com.xmb.taobaoke.web.XMBApiCallback;

import okhttp3.Response;

public class MainActivity extends BaseAppCompatActivity {
    ServerConfigBean cofigBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void onClickOpen4Act(View v) {
        TBKMain.start4Activity(this);
    }


    public void onClickOpen4FragNew(View v) {
        TBKIndexFragment fragment = TBKMain.start4Fragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, fragment);
        transaction.commitAllowingStateLoss();
    }


}
